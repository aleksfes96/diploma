compile_options = --output-directory=pdf --interaction=nonstopmode
sources = pz.tex tz.tex rsp.tex

clean_compile: common.tex settings.tex $(sources) compile clean

compile: common.tex settings.tex $(sources)
# Компилируем трижды, чтобы корректно обработались все ссылки (специфика работы  LuaLaTeX)
	for source in $(sources) ; do \
		lualatex $(compile_options) $$source ; \
		lualatex $(compile_options) $$source ; \
		lualatex $(compile_options) $$source ; \
	done

.PHONY: clean
clean:
	@-rm -f ./pdf/*.log *.log
	@-rm -f ./pdf/*.toc *.toc
	@-rm -f ./pdf/*.aux *.aux
	@-rm -f ./pdf/*.nav *.nav
	@-rm -f ./pdf/*.out *.out
	@-rm -f ./pdf/*.snm *.snm
	@-rm -f ./pdf/*.vrb *.vrb

	@-rm -f ./*.log *.log
	@-rm -f ./*.toc *.toc
	@-rm -f ./*.aux *.aux
	@-rm -f ./*.nav *.nav
	@-rm -f ./*.out *.out
	@-rm -f ./*.snm *.snm
	@-rm -f ./*.vrb *.vrb

.PHONY: clear
clear: clean
	@-rm -f ./pdf/*.pdf *.pdf
	@-rm -f ./pdf/*.bak *.bak 
