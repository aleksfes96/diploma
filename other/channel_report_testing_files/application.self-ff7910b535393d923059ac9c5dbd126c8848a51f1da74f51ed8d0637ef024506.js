











































CKEDITOR.config.allowedContent = true;
CKEDITOR.config.scayt_autoStartup = false; // отключает автопроверку орфографии плагином scayt
CKEDITOR.config.disableNativeSpellChecker = false; // отключает запрет на стандартную проверку орфографии браузером
CKEDITOR.config.removePlugins = 'scayt'; // отключает плагин scayt
CKEDITOR.config.baseFloatZIndex = 100001; // Фикс таблиц в модальных окнах

CKEDITOR.toolbar_mini = [
    // Хочешь добавить кнопочки? Зырь тут: http://docs.cksource.com/CKEditor_3.x/Developers_Guide/Toolbar
    { name: 'document', items : [ 'Source', '-', 'PasteFromWord', 'Undo','Redo', 'Image' ,'Table', 'SpecialChar' ] },

        '/',
    { name: 'styles', items : ['Format'] },
    { name: 'basicstyles', items : [ 'Bold','Italic','Strike','-','RemoveFormat' ] },
    { name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-' ] },
    { name: 'links', items : [ 'Link','Unlink','Anchor' ] },
    { name: 'tools', items : [ 'Maximize'] }
    ];
CKEDITOR.on( 'dialogDefinition', function( ev ) {
    var dialogName = ev.data.name;
    var dialogDefinition = ev.data.definition;

    if ( dialogName == 'table' ) {
        var info = dialogDefinition.getContents( 'info' );

        info.get( 'txtWidth' )[ 'default' ] = '';
        info.get( 'txtBorder' )[ 'default' ] = '';
        info.get( 'txtCellSpace' )[ 'default' ] = '';
        info.get( 'txtCellPad' )[ 'default' ] = '';
    }
});
CKEDITOR.toolbar_smini = [
    // Хочешь добавить кнопочки? Зырь тут: http://docs.cksource.com/CKEditor_3.x/Developers_Guide/Toolbar
    { name: 'document', items : [ 'Source', 'Table', 'SpecialChar' ] },
    { name: 'basicstyles', items : ['Bold','Italic'] },
    { name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent' ] },
    { name: 'links', items : [ 'Link','Unlink', '-', 'Format' ] }
];
CKEDITOR.on( 'dialogDefinition', function( ev ) {
    var dialogName = ev.data.name;
    var dialogDefinition = ev.data.definition;

    if ( dialogName == 'table' ) {
        var info = dialogDefinition.getContents( 'info' );

        info.get( 'txtWidth' )[ 'default' ] = '';
        info.get( 'txtBorder' )[ 'default' ] = '';
        info.get( 'txtCellSpace' )[ 'default' ] = '';
        info.get( 'txtCellPad' )[ 'default' ] = '';
    }
});

Handlebars.registerHelper("?", function(value, yes, no) {
    return value ? yes : no
})

Handlebars.registerHelper('select', function( value, options ){
    var $el = $('<select />').html(options.fn(this))
    $el.find('[value="' + value + '"]').attr({'selected':'selected'})
    return $el.html()
})

Handlebars.registerHelper("math", function(lvalue, operator, rvalue, options) {
    lvalue = parseFloat(lvalue);
    rvalue = parseFloat(rvalue);
        
    return {
        "+": lvalue + rvalue,
        "-": lvalue - rvalue,
        "*": lvalue * rvalue,
        "/": lvalue / rvalue,
        "%": lvalue % rvalue
    }[operator];
});

// делает так, чтобы дата при приеме на сервере никогда не сбивалась из-за разницы в часовых поясах
function fixGmtDateBeforeSendToServer(gmtIsoDate){
   if(gmtIsoDate == undefined || gmtIsoDate == null || gmtIsoDate == '')
     return null;
   return new Date(Date.UTC(gmtIsoDate.getFullYear(), gmtIsoDate.getMonth(), gmtIsoDate.getDate()));
}

// для легкого обращение к get параметрам через $.urlParam('param_name');
$.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
        return null;
    }
    else{
        return results[1] || 0;
    }
};

// Функция применяющая маску к инпутам для телефонов
// используется либо маска из аттрибута mask либо стандартная маска
// для применения маски необходимо наличие класса masked-phone у инпута
// например
// <input class="form-control masked-phone" mask="+7? (9999) 99-99-99 " id="region_phone" name="region[phone]" type="text" value="8 800 555 3535"/>
// применит маску вида +7 (____) __-__-__ под городской телефон с 4 значным кодом
maskMany = function(){
  $('input.masked-phone').each(function(){
    applyMask($(this))
  });
};

applyMask = function(selector){
  default_mask = "+7? (999) 999-99-99";
  var mask = $(selector).attr('mask') || default_mask;
  $(selector).mask(mask);
  $(selector).change();
};

$(function(){
  maskMany()
});

// Функция для рендеринга сообщений об ошибке
// Ошибки рендерятся в блок с идентификатором alerts
// msg - строка с сообщением об ошибке
render_alert_msg = function(msg, type, $dom)
{
    if(typeof msg == 'undefined')
        return;

    var alerts_block = typeof $dom == 'undefined' ? $('#alerts') : $dom;
    type = typeof type == 'undefined' ? 'danger' : type;

    var icon_class = '';
    switch (type)
    {
        case 'warning':
            icon_class = 'glyphicon glyphicon-exclamation-sign';
            break;
        case 'success':
            icon_class = 'glyphicon glyphicon-ok-sign';
            break;
        case 'danger':
            icon_class = 'glyphicon glyphicon-remove-sign';
            break;
        case 'info':
        default:
            icon_class = 'glyphicon glyphicon-info-sign';
    }

    alerts_block.append(
        '<alert class="alert alert-' + type + '" style="margin-left: 15%; margin-right: 15%; display: block;" type="danger"> ' +
        '<a href="#" class="close" data-dismiss="alert" onclick="$(this).closest(\"alert\").remove()">&times;</a> ' +
        '<span class="' + icon_class + '"></span> ' +
        msg +
        '</alert>');
};

// Функция для получения значения get-параметра из строки поиска по ключу
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
