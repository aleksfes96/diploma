(function() {
  var check_positive_nimber, controllers, error_column_color, initFormParams, save, validated_dates, validated_days;

  controllers = angular.module('extranet.controllers', ['colorpicker.module']);

  error_column_color = '#f2dede';

  save = function($scope, $http, sucess_message, error_message) {
    var promise;
    promise = $http.put("/apartments_update", {
      apartments: $scope.apartments
    });
    $scope.message = 'Обновление...';
    $scope.additionally_message = '';
    $scope.status = 'info';
    promise.success(function(data) {
      $scope.apartments = [];
      angular.forEach(data.apartments, function(apartment) {
        var apartment_ui;
        apartment_ui = {};
        apartment_ui['id'] = apartment.id;
        apartment_ui['name'] = apartment.name;
        apartment_ui['count_of'] = apartment.count_of;
        apartment_ui['seats'] = apartment.seats;
        apartment_ui['seats_additional'] = apartment.seats_additional;
        apartment_ui['is_request_booking'] = apartment.is_request_booking;
        apartment_ui['options'] = apartment.options;
        apartment_ui['count_of_css'] = {};
        apartment_ui['seats_css'] = {};
        apartment_ui['seats_additional_css'] = {};
        return $scope.apartments.push(apartment_ui);
      });
      $scope.status = 'success';
      $scope.message = sucess_message[0];
      $scope.additionally_message = sucess_message[1];
      return $scope.send_disabled = false;
    });
    promise.error(function(data) {
      $scope.status = 'danger';
      $scope.message = error_message[0];
      $scope.additionally_message = error_message[1];
      $scope.errors = data;
      return $scope.send_disabled = false;
    });
    return window.scrollTo(0, 0);
  };

  check_positive_nimber = function(str) {
    return /^(0|[1-9]\d*)$/.test(str) || str === '';
  };

  validated_dates = function(start_date, finish_date) {
    return start_date < finish_date;
  };

  validated_days = function(days) {
    var valide;
    valide = false;
    angular.forEach(days, function(value) {
      if (value === true) {
        return valide = true;
      }
    });
    return valide;
  };

  initFormParams = function($scope) {
    $scope.camp_id = $scope.$parent.json.camp.id;
    $scope.send_disabled = true;
    $scope.alerts = [];
    $scope.closeAlert = function(index) {
      return $scope.alerts.splice(index, 1);
    };
    $scope.format = 'dd MMMM yyyy';
    $scope.days_rus = ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'];
    $scope.quotas_params = {};
    $scope.quotas_params["delete"] = false;
    $scope.dateOptions = {
      'year-format': "'yy'",
      'starting-day': 1,
      'show-weeks': false
    };
    $scope.current_dt = new Date();
    $scope.start_date = $scope.current_dt;
    $scope.finish_date = new Date($scope.current_dt.getFullYear(), $scope.current_dt.getMonth() + 3, $scope.current_dt.getDate());
    $scope.right_limit = new Date($scope.current_dt.getFullYear(), $scope.current_dt.getMonth() + 15, $scope.current_dt.getDate());
    $scope.finish_date_right_limit = new Date();
    return $scope.finish_date_right_limit.setDate($scope.current_dt.getDate() + 1);
  };

  controllers.filter('optionsReverse', function() {
    return function(items) {
      return items.slice().reverse();
    };
  });

  controllers.controller('campRoomsFormController', function($location, $scope, $http, $rootScope) {
    var invert_is_request_booking, promise, validated, validated_count_of, validated_seats, validated_seats_additional;
    $scope.send_disabled = true;
    promise = $http.get('');
    promise.success(function(data, status) {
      $scope.send_disabled = false;
      $scope.apartments = data.apartments;
      return invert_is_request_booking();
    });
    $scope.save = function() {
      var error_messages, sucess_messages;
      $scope.send_disabled = true;
      if (validated()) {
        sucess_messages = ['Номерной фонд обновлен.', ''];
        error_messages = ['Не удалось обновить номерной фонд.', 'Пожалуйста, проверьте еще раз введенные данные или обратитесь к разработчикам.'];
        return save($scope, $http, sucess_messages, error_messages);
      } else {
        return $scope.send_disabled = false;
      }
    };
    $scope.total_count_of = function() {
      var total_count_of;
      total_count_of = 0;
      angular.forEach($scope.apartments, function(apartment) {
        return total_count_of += apartment.count_of;
      });
      return total_count_of;
    };
    $scope.total_seats = function() {
      var total_seats;
      total_seats = 0;
      angular.forEach($scope.apartments, function(apartment) {
        return total_seats += apartment.seats;
      });
      return total_seats;
    };
    $scope.total_seats_additional = function() {
      var total_seats_additional;
      total_seats_additional = 0;
      angular.forEach($scope.apartments, function(apartment) {
        return total_seats_additional += apartment.seats_additional;
      });
      return total_seats_additional;
    };
    save = function($scope, $http, sucess_message, error_message) {
      invert_is_request_booking();
      promise = $http.put("/apartments_update", {
        apartments: $scope.apartments
      });
      $scope.message = 'Обновление...';
      $scope.additionally_message = '';
      $scope.status = 'info';
      promise.success(function(data) {
        $scope.apartments = [];
        angular.forEach(data.apartments, function(apartment) {
          var apartment_ui;
          apartment_ui = {};
          apartment_ui['id'] = apartment.id;
          apartment_ui['name'] = apartment.name;
          apartment_ui['count_of'] = apartment.count_of;
          apartment_ui['seats'] = apartment.seats;
          apartment_ui['seats_additional'] = apartment.seats_additional;
          apartment_ui['is_request_booking'] = apartment.is_request_booking;
          apartment_ui['options'] = apartment.options;
          apartment_ui['count_of_css'] = {};
          apartment_ui['seats_css'] = {};
          apartment_ui['seats_additional_css'] = {};
          return $scope.apartments.push(apartment_ui);
        });
        invert_is_request_booking();
        $scope.status = 'success';
        $scope.message = sucess_message[0];
        $scope.additionally_message = sucess_message[1];
        return $scope.send_disabled = false;
      });
      promise.error(function(data) {
        $scope.status = 'danger';
        $scope.message = error_message[0];
        $scope.additionally_message = error_message[1];
        $scope.errors = data;
        return $scope.send_disabled = false;
      });
      return window.scrollTo(0, 0);
    };
    invert_is_request_booking = function() {
      return angular.forEach($scope.apartments, function(apartment) {
        return apartment.is_request_booking = !apartment.is_request_booking;
      });
    };
    validated = function() {
      var valide;
      valide = true;
      if (!validated_count_of()) {
        $scope.message = "Количество номеров задано неверно.";
        valide = false;
      }
      if (!validated_seats()) {
        $scope.message = "Количество основных мест задано неверно.";
        valide = false;
      }
      if (!validated_seats_additional()) {
        $scope.message = "Количество дополнительных мест задано неверно.";
        valide = false;
      }
      if (!valide) {
        $scope.status = 'danger';
        $scope.additionally_message = "Должно быть указано положительное число от 0 и больше. Пожалуйста, проверьте еще раз введенные данные или обратитесь к разработчикам.";
      }
      return valide;
    };
    validated_count_of = function() {
      var valide;
      valide = true;
      angular.forEach($scope.apartments, function(apartment) {
        apartment.count_of_css = {};
        if (!check_positive_nimber(apartment.count_of)) {
          valide = false;
          return apartment.count_of_css = {
            'background-color': error_column_color
          };
        }
      });
      return valide;
    };
    validated_seats = function() {
      var valide;
      valide = true;
      angular.forEach($scope.apartments, function(apartment) {
        apartment.seats_css = {};
        if (!check_positive_nimber(apartment.seats)) {
          valide = false;
          return apartment.seats_css = {
            'background-color': error_column_color
          };
        }
      });
      return valide;
    };
    return validated_seats_additional = function() {
      var valide;
      valide = true;
      angular.forEach($scope.apartments, function(apartment) {
        apartment.seats_additional_css = {};
        if (!check_positive_nimber(apartment.seats_additional)) {
          valide = false;
          return apartment.seats_additional_css = {
            'background-color': error_column_color
          };
        }
      });
      return valide;
    };
  });

  controllers.controller('campApartmentsFormController', function($location, $scope, $http, $rootScope) {
    var promise;
    $scope.send_disabled = true;
    promise = $http.get('');
    promise.success(function(data, status) {
      var i, results;
      $scope.send_disabled = false;
      $scope.apartments = data.apartments;
      i = 0;
      results = [];
      while (i < $scope.apartments.length) {
        $scope.check_options_buttons($scope.apartments[i]);
        results.push(++i);
      }
      return results;
    });
    $scope.save = function() {
      var error_messages, sucess_messages;
      $scope.send_disabled = true;
      sucess_messages = ['Виды размещений обновлены.', ''];
      error_messages = ['Не удалось обновить виды размещений.', ''];
      return save($scope, $http, sucess_messages, error_messages);
    };
    $scope.check_options_buttons = function(apartment) {
      var add_btn_name, remove_btn_name;
      remove_btn_name = 'remove_option_button_' + apartment.id;
      add_btn_name = 'add_option_button_' + apartment.id;
      if (apartment.options && apartment.options.length < 2) {
        $scope[remove_btn_name] = true;
      } else {
        $scope[remove_btn_name] = false;
      }
      if (apartment.options && apartment.options[apartment.options.length - 1] === 1) {
        $scope[add_btn_name] = true;
      } else {
        $scope[add_btn_name] = false;
      }
      if (apartment.seats === void 0) {
        $scope[remove_btn_name] = true;
        return $scope[add_btn_name] = true;
      }
    };
    $scope.add_option = function(apartment) {
      var value_to_push;
      value_to_push = apartment.options[apartment.options.length - 1] - 1;
      apartment.options.push(value_to_push);
      return $scope.check_options_buttons(apartment);
    };
    return $scope.remove_option = function(apartment) {
      apartment.options.pop();
      return $scope.check_options_buttons(apartment);
    };
  });

  controllers.controller('campQuotasFormController', function($scope, $http, $timeout, $sce) {
    var validated, validated_quotas, validated_quotas_count;
    initFormParams($scope);
    $scope.quotas_params.days = [true, true, true, true, true, true, true];
    $scope.quotas_params.apartments = [];
    $scope.quotas_params.camp_id = $scope.$parent.json.camp.id;
    $scope.quotas_params.start_date = $scope.current_dt;
    $scope.quotas_params.finish_date = new Date($scope.current_dt.getFullYear(), $scope.current_dt.getMonth(), $scope.current_dt.getDate() + 14);
    angular.forEach($scope.$parent.json.camp.apartments, function(apartment) {
      var apartment_ui;
      apartment_ui = {};
      if (apartment.is_request_booking === false) {
        apartment_ui['id'] = apartment.id;
        apartment_ui['options'] = {};
        apartment_ui['count_of'] = apartment.count_of;
        angular.forEach(apartment.options, function(option) {
          apartment_ui['name'] = apartment.name;
          return apartment_ui['quota'] = '';
        });
        return $scope.quotas_params.apartments.push(apartment_ui);
      }
    });
    $scope.send = function() {
      $scope.send_disabled = true;
      return $http({
        url: "/quotas/" + $scope.camp_id,
        method: "POST",
        params: {
          start_date: $scope.start_date,
          finish_date: $scope.finish_date
        }
      }).success(function(data) {
        $('#apartment_container').html(data);
        return $scope.send_disabled = false;
      }).error(function(data) {
        $scope.send_disabled = false;
        $scope.status = 'danger';
        $scope.message = 'Произошла ошибка.';
        return $scope.additionally_message = 'Попробуйте еще раз или обратитесь к разработчикам.';
      });
    };
    $scope.quotas_params.send = function() {
      if (validated()) {
        $scope.send_disabled = true;
        $('#myModal').modal('hide');
        return $http.put('/quotas/update', {
          quota: this
        }).success(function(data) {
          $scope.send_disabled = false;
          $timeout(function() {
            return $('#refresh_btn').click();
          });
          $scope.status = 'success';
          $scope.message = 'Операция редактирования успешно завершена.';
          $scope.additionally_message = '';
          return $scope.cancel();
        }).error(function(data) {
          $scope.send_disabled = false;
          $scope.modal_status = 'danger';
          $scope.modal_message = "Произошла ошибка.";
          return $scope.modal_additionally_message = "Попробуйте еще раз или обратитесь к разработчикам.";
        });
      } else {
        return $('#myModal').scrollTop(0);
      }
    };
    $scope.cancel = function() {
      $scope.modal_status = '';
      $scope.modal_message = '';
      $scope.modal_additionally_message = '';
      return angular.forEach($scope.quotas_params.apartments, function(apartment) {
        apartment.quota = '';
        return apartment.quota_css = {};
      });
    };
    $scope.activate_days = function(type) {
      if (type === 'all') {
        return $scope.quotas_params.days = [true, true, true, true, true, true, true];
      } else if (type === 'weekend') {
        return $scope.quotas_params.days = [false, false, false, false, false, true, true];
      } else if (type === 'working') {
        return $scope.quotas_params.days = [true, true, true, true, true, false, false];
      }
    };
    validated = function() {
      var valide;
      valide = true;
      if (!validated_dates($scope.quotas_params.start_date, $scope.quotas_params.finish_date)) {
        $scope.modal_status = 'danger';
        $scope.modal_message = 'Ошибка.';
        $scope.modal_additionally_message = "Дата \"по\" должна быть больше, чем дата \"с\".";
        valide = false;
      }
      if (!validated_days($scope.quotas_params.days)) {
        $scope.modal_status = 'danger';
        $scope.modal_message = 'Ошибка.';
        $scope.modal_additionally_message = "Не выбрано ни одного дня.";
        valide = false;
      }
      if (!validated_quotas()) {
        $scope.modal_status = 'danger';
        $scope.modal_message = 'Ошибка.';
        $scope.modal_additionally_message = "Квота задана неверно.";
        valide = false;
      } else if (!validated_quotas_count()) {
        $scope.modal_status = 'danger';
        $scope.modal_message = 'Ошибка.';
        $scope.modal_additionally_message = $sce.trustAsHtml("Квота больше заданного количества домиков/номеров. Увеличьте их количество в разделе <a target=\"_blank\" href=\"" + window.location.href.replace(/camp_quotas/, 'camp_rooms') + "\">номерной фонд</a> или уменьшите квоту.");
        valide = false;
      }
      return valide;
    };
    validated_quotas = function() {
      var valide;
      valide = true;
      angular.forEach($scope.quotas_params.apartments, function(apartment) {
        apartment.quota_css = {};
        if (!check_positive_nimber(apartment.quota)) {
          valide = false;
          return apartment.quota_css = {
            'background-color': error_column_color
          };
        }
      });
      return valide;
    };
    validated_quotas_count = function() {
      var valide;
      valide = true;
      angular.forEach($scope.quotas_params.apartments, function(apartment) {
        apartment.quota_css = {};
        if (parseInt(apartment.quota) > apartment.count_of && apartment.quota !== '') {
          valide = false;
          return apartment.quota_css = {
            'background-color': error_column_color
          };
        }
      });
      return valide;
    };
    return $scope.send();
  });

  controllers.controller('campPricesFormController', function($scope, $http, $timeout) {
    var validated, validated_price;
    initFormParams($scope);
    $scope.prices_params = {};
    $scope.prices_params["delete"] = false;
    $scope.prices_params.start_date = $scope.current_dt;
    $scope.prices_params.finish_date = new Date($scope.current_dt.getFullYear(), $scope.current_dt.getMonth(), $scope.current_dt.getDate() + 14);
    $scope.prices_params.days = [true, true, true, true, true, true, true];
    $scope.prices_params.apartments = [];
    $scope.prices_params.camp_id = $scope.$parent.json.camp.id;
    $scope.prices_params.camp_currency_name = $scope.$parent.json.camp.currency_name;
    angular.forEach($scope.$parent.json.camp.apartments, function(apartment) {
      var apartment_ui;
      apartment_ui = {};
      apartment_ui['id'] = apartment.id;
      apartment_ui['options'] = {};
      angular.forEach(apartment.options, function(option) {
        return apartment_ui['options'][option] = '';
      });
      apartment_ui['options_css'] = {};
      angular.forEach(apartment.options, function(option) {
        return apartment_ui['options_css'][option] = {};
      });
      apartment_ui['additional_place'] = false;
      apartment_ui['name'] = apartment.name;
      apartment_ui['pricing_type'] = apartment.pricing_type;
      apartment_ui['seats_additional'] = apartment.seats_additional;
      apartment_ui['seats_additional_price'] = '';
      apartment_ui['bed_price'] = '';
      apartment_ui['seats_additional_price_css'] = {};
      apartment_ui['bed_price_css'] = {};
      return $scope.prices_params.apartments.push(apartment_ui);
    });
    $scope.send = function() {
      $scope.send_disabled = true;
      return $http({
        url: "/prices/" + $scope.camp_id,
        method: "POST",
        params: {
          start_date: $scope.start_date,
          finish_date: $scope.finish_date
        }
      }).success(function(data) {
        $('#apartment_container').html(data);
        return $scope.send_disabled = false;
      }).error(function(data) {
        $scope.send_disabled = false;
        $scope.status = 'danger';
        $scope.message = 'Произошла ошибка.';
        return $scope.additionally_message = 'Попробуйте еще раз или обратитесь к разработчикам.';
      });
    };
    $scope.prices_params.send = function() {
      if (validated()) {
        $scope.send_disabled = true;
        $('#myModal').modal('hide');
        return $http.put('/prices/extranet_update', {
          price: this
        }).success(function(data) {
          $scope.send_disabled = false;
          $timeout(function() {
            return $('#refresh_btn').click();
          });
          $scope.status = 'success';
          $scope.message = "Операция редактирования успешно завершена.";
          $scope.additionally_message = "";
          return $scope.cancel();
        }).error(function(data) {
          $scope.send_disabled = false;
          $scope.modal_status = 'danger';
          $scope.modal_message = "Произошла ошибка.";
          return $scope.modal_additionally_message = "Попробуйте еще раз или обратитесь к разработчикам.";
        });
      } else {
        return $('#myModal').scrollTop(0);
      }
    };
    $scope.cancel = function() {
      $scope.modal_status = '';
      $scope.modal_message = "";
      $scope.modal_additionally_message = "";
      return angular.forEach($scope.prices_params.apartments, function(apartment) {
        angular.forEach(apartment.options, function(value, key) {
          apartment.options[key] = '';
          return apartment.options_css[key] = {};
        });
        apartment.seats_additional_price = '';
        apartment.bed_price = '';
        apartment.seats_additional_price_css = {};
        return apartment.bed_price_css = {};
      });
    };
    $scope.activate_days = function(type) {
      if (type === 'all') {
        return $scope.prices_params.days = [true, true, true, true, true, true, true];
      } else if (type === 'weekend') {
        return $scope.prices_params.days = [false, false, false, false, false, true, true];
      } else if (type === 'working') {
        return $scope.prices_params.days = [true, true, true, true, true, false, false];
      }
    };
    validated = function() {
      var valide;
      valide = true;
      if (!validated_dates($scope.prices_params.start_date, $scope.prices_params.finish_date)) {
        $scope.modal_status = 'danger';
        $scope.modal_message = 'Ошибка.';
        $scope.modal_additionally_message = "Дата \"по\" должна быть больше, чем дата \"с\".";
        valide = false;
      }
      if (!validated_days($scope.prices_params.days)) {
        $scope.modal_status = 'danger';
        $scope.modal_message = 'Ошибка.';
        $scope.modal_additionally_message = "Не выбрано ни одного дня.";
        valide = false;
      }
      if (!validated_price()) {
        $scope.modal_status = 'danger';
        $scope.modal_message = 'Ошибка.';
        $scope.modal_additionally_message = "Цена задана неверно.";
        valide = false;
      }
      return valide;
    };
    validated_price = function() {
      var valide;
      valide = true;
      angular.forEach($scope.prices_params.apartments, function(apartment) {
        var i;
        apartment.seats_additional_price_css = {};
        apartment.bed_price_css = {};
        i = 0;
        angular.forEach(apartment.options, function(option, key) {
          apartment.options_css[key] = {};
          if (!check_positive_nimber(option)) {
            valide = false;
            apartment.options_css[key] = {
              'background-color': error_column_color
            };
          }
          return ++i;
        });
        if (!check_positive_nimber(apartment.seats_additional_price)) {
          valide = false;
          apartment.seats_additional_price_css = {
            'background-color': error_column_color
          };
        }
        if (!check_positive_nimber(apartment.bed_price)) {
          valide = false;
          return apartment.bed_price_css = {
            'background-color': error_column_color
          };
        }
      });
      return valide;
    };
    return $scope.send();
  });

  controllers.controller('campStopSaleFormController', function($scope, $http, $timeout) {
    initFormParams($scope);
    $scope.stop_sale_params = {};
    $scope.stop_sale_params.days = [true, true, true, true, true, true, true];
    $scope.stop_sale_params.apartments = [];
    $scope.stop_sale_params.camp_id = $scope.$parent.json.camp.id;
    $scope.stop_sale_params.start_date = $scope.current_dt;
    $scope.stop_sale_params.finish_date = new Date($scope.current_dt.getFullYear(), $scope.current_dt.getMonth(), $scope.current_dt.getDate() + 14);
    angular.forEach($scope.$parent.json.camp.apartments, function(apartment) {
      var apartment_ui;
      apartment_ui = {};
      apartment_ui['id'] = apartment.id;
      apartment_ui['options'] = {};
      angular.forEach(apartment.options, function(option) {
        apartment_ui['name'] = apartment.name;
        return apartment_ui['stop_sale'] = '';
      });
      return $scope.stop_sale_params.apartments.push(apartment_ui);
    });
    $scope.send = function() {
      $scope.send_disabled = true;
      return $http({
        url: "/stop_sales/" + $scope.camp_id,
        method: "POST",
        params: {
          start_date: $scope.start_date,
          finish_date: $scope.finish_date
        }
      }).success(function(data) {
        $('#apartment_container').html(data);
        return $scope.send_disabled = false;
      }).error(function(data) {
        $scope.send_disabled = false;
        $scope.status = 'danger';
        $scope.message = 'Произошла ошибка.';
        return $scope.additionally_message = 'Попробуйте еще раз или обратитесь к разработчикам.';
      });
    };
    $scope.stop_sale_params.send = function() {
      $scope.send_disabled = true;
      $('#myModal').modal('hide');
      return $http.put('/stop_sales/update', {
        stop_sale: this
      }).success(function(data) {
        $scope.send_disabled = false;
        $timeout(function() {
          return $('#refresh_btn').click();
        });
        $scope.status = 'success';
        $scope.message = 'Операция редактирования успешно завершена.';
        $scope.additionally_message = '';
        return $scope.cancel();
      }).error(function(data) {
        $scope.send_disabled = false;
        $scope.modal_status = 'danger';
        $scope.modal_message = "Произошла ошибка.";
        return $scope.modal_additionally_message = "Попробуйте еще раз или обратитесь к разработчикам.";
      });
    };
    $scope.cancel = function() {
      $scope.modal_status = '';
      $scope.modal_message = '';
      $scope.modal_additionally_message = '';
      return angular.forEach($scope.stop_sale_params.apartments, function(apartment) {
        return apartment.stop_sale = '';
      });
    };
    $scope.activate_days = function(type) {
      if (type === 'all') {
        return $scope.stop_sale_params.days = [true, true, true, true, true, true, true];
      } else if (type === 'weekend') {
        return $scope.stop_sale_params.days = [false, false, false, false, false, true, true];
      } else if (type === 'working') {
        return $scope.stop_sale_params.days = [true, true, true, true, true, false, false];
      }
    };
    return $scope.send();
  });

  controllers.controller('adminFormController', function($scope, $http) {
    $scope.send_disabled = false;
    $scope.format = 'dd MMMM yyyy';
    $scope.dateOptions = {
      'year-format': "'yy'",
      'starting-day': 1,
      'show-weeks': false
    };
    $scope.current_dt = new Date();
    $scope.start_date = $scope.current_dt;
    $scope.finish_date = new Date($scope.current_dt.getFullYear(), $scope.current_dt.getMonth() + 3, $scope.current_dt.getDate());
    $scope.right_limit = new Date($scope.current_dt.getFullYear(), $scope.current_dt.getMonth() + 15, $scope.current_dt.getDate());
    $scope.finish_date_right_limit = new Date();
    $scope.finish_date_right_limit.setDate($scope.current_dt.getDate() + 1);
    $scope.params = {};
    $scope.params.start_date = $scope.start_date;
    return $scope.params.finish_date = $scope.finish_date;
  });

  controllers.filter('query', function() {
    return function(opts) {
      var opt, params;
      params = [];
      for (opt in opts) {
        if (opts.hasOwnProperty(opt)) {
          if (opts[opt] !== '' && opts[opt] !== void 0) {
            params.push(opt + '=' + opts[opt]);
          }
        }
      }
      if (params.length) {
        return '?' + params.join('&');
      } else {
        return '';
      }
    };
  });

  controllers.controller('statisticFormController', function($scope, $http, $timeout) {
    $scope.send_disabled = false;
    $scope.format = 'dd MMMM yyyy';
    $scope.dateOptions = {
      'year-format': "'yy'",
      'starting-day': 1,
      'show-weeks': false
    };
    $scope.current_dt = new Date();
    $scope.start_date = $scope.current_dt;
    $scope.finish_date = new Date($scope.current_dt.getFullYear(), $scope.current_dt.getMonth() + 3, $scope.current_dt.getDate());
    $scope.update = function(type, camp_id, region_id) {
      if (camp_id == null) {
        camp_id = null;
      }
      if (region_id == null) {
        region_id = null;
      }
      $scope.get_prices_statistic(type, camp_id, region_id);
      return $scope.get_quotas_statistic(type, camp_id, region_id);
    };
    $scope.get_prices_statistic = function(type, camp_id, region_id) {
      if (camp_id == null) {
        camp_id = null;
      }
      if (region_id == null) {
        region_id = null;
      }
      $scope.send_disabled = true;
      return $http({
        url: "/prices_statistic",
        method: "POST",
        params: {
          start_date: $scope.start_date,
          finish_date: $scope.finish_date,
          type: type,
          camp_id: camp_id,
          region_id: region_id
        }
      }).success(function(data) {
        $('#prices_container').html(data);
        return $scope.send_disabled = false;
      }).error(function(data) {
        $scope.send_disabled = false;
        $scope.status = 'danger';
        $scope.message = 'Произошла ошибка.';
        return $scope.additionally_message = 'Попробуйте еще раз или обратитесь к разработчикам.';
      });
    };
    $scope.get_quotas_statistic = function(type, camp_id, region_id) {
      if (camp_id == null) {
        camp_id = null;
      }
      if (region_id == null) {
        region_id = null;
      }
      $scope.send_disabled = true;
      return $http({
        url: "/quotas_statistic",
        method: "POST",
        params: {
          start_date: $scope.start_date,
          finish_date: $scope.finish_date,
          type: type,
          camp_id: camp_id,
          region_id: region_id
        }
      }).success(function(data) {
        $('#quotas_container').html(data);
        return $scope.send_disabled = false;
      }).error(function(data) {
        $scope.send_disabled = false;
        $scope.status = 'danger';
        $scope.message = 'Произошла ошибка.';
        return $scope.additionally_message = 'Попробуйте еще раз или обратитесь к разработчикам.';
      });
    };
    $scope.set = function() {
      return $timeout(function() {
        return $('#refresh_btn').click();
      });
    };
    return $scope.set();
  });

  controllers.controller('campBooksFormController', function($scope, $http) {
    $scope.accept_book = function(is_accept, book_id) {
      $scope.send_disabled = true;
      if (is_accept) {
        $scope.update_book_status(true, '', book_id);
      } else {
        $('#cancellation_reason_' + book_id).css('display', 'block');
      }
    };
    $scope.decline_book = function(book_id) {
      $('#cancellation_reason_' + book_id).css('display', 'none');
      $scope.update_book_status(false, $('#zero_reason_message_' + book_id)[0].value, book_id);
      $('#zero_reason_message_' + book_id)[0].value = '';
    };
    $scope.update_book_status = function(is_accept, cancellation_reason, book_id) {
      if (is_accept == null) {
        is_accept = true;
      }
      if (cancellation_reason == null) {
        cancellation_reason = '';
      }
      $http({
        url: "/accept_book",
        method: "POST",
        params: {
          is_accept: is_accept,
          cancellation_reason: cancellation_reason,
          book_id: book_id
        }
      }).success(function(data) {
        $scope.send_disabled = false;
        $('#buttons_' + book_id).css('display', 'none');
        if (is_accept) {
          return $('#accepted_true_' + book_id).css('display', 'inline-block');
        } else {
          return $('#accepted_false_' + book_id).css('display', 'inline-block');
        }
      }).error(function(data) {
        $scope.send_disabled = false;
        $scope.status = 'danger';
        $scope.message = 'Произошла ошибка.';
        return $scope.additionally_message = 'Попробуйте еще раз или обратитесь к разработчикам.';
      });
    };
    return $scope.cancel = function(book_id) {
      $('#cancellation_reason_' + book_id).css('display', 'none');
      $('#zero_reason_message_' + book_id)[0].value = '';
    };
  });

  controllers.controller('campAccommodationsFormController', function($scope, $http) {
    $scope.accommodation = $scope.json.accommodation;
    return $scope.update_accommodation = function() {
      var promise;
      $scope.update_status = 'processing';
      promise = $http.put('/update_accommodations', {
        accommodation: $scope.accommodation
      });
      promise.success(function(data) {
        $scope.accommodation = data.accommodation;
        return $scope.update_status = Object.keys(data.errors).length === 0 ? 'success' : 'fail';
      });
      return promise.error(function(data) {
        $scope.update_status = 'fail';
        return console.log(data);
      });
    };
  });

  controllers.controller('campReviewsFormController', function($scope, $http) {
    $scope.comment_review = function(review_id) {
      $('#comment_' + review_id).css('display', 'block');
    };
    $scope.update_comment = function(review_id) {
      $http({
        url: "/comment_review",
        method: "POST",
        params: {
          review_id: review_id,
          comment: $('#review_comment_' + review_id).val()
        }
      }).success(function(data) {
        $("#comment_review_" + review_id).text($('#review_comment_' + review_id).val());
        return $scope.cancel(review_id);
      }).error(function(data) {
        $scope.status = 'danger';
        $scope.message = 'Произошла ошибка.';
        return $scope.additionally_message = 'Попробуйте еще раз или обратитесь к разработчикам.';
      });
    };
    return $scope.cancel = function(review_id) {
      return $('#comment_' + review_id).css('display', 'none');
    };
  });

  controllers.controller('campBookingWidgetFormController', function($scope, $http, $window) {
    var interval, promise, slider_dragged, vertical_elem;
    vertical_elem = $('#ex2Slider > div.slider-handle.min-slider-handle.round');
    $scope.send_disabled = false;
    $scope.status = '';
    promise = $http.get('');
    promise.success(function(data, status) {
      $scope.is_use_booking_widget = data.is_use_booking_widget;
      $scope.partner_enabled = data.partner_enabled;
      $scope.booking_widget_type = data.booking_widget_type;
      $scope.booking_widget_settings = data.booking_widget_settings;
      $scope.booking_widget_id = data.booking_widget_id;
      $scope.booking_widget_bg_color = data.booking_widget_bg_color;
      $scope.booking_widget_button_color = data.booking_widget_button_color;
      $scope.booking_widget_border_radius = data.booking_widget_border_radius;
      $scope.booking_widget_width = data.booking_widget_width;
      $scope.booking_widget_width_responsive = data.booking_widget_width_responsive;
      $scope.booking_widget_header_color = data.booking_widget_header_color;
      $scope.booking_widget_button_font_color = data.booking_widget_button_font_color;
      $scope.booking_widget_width_tmp = data.booking_widget_width;
      return $scope.restyle_widget();
    });
    $scope.update_colors = function() {
      return setTimeout((function() {
        $scope.booking_widget_bg_color = $('#color_container label.active').attr('data-bg-color');
        $scope.booking_widget_button_color = $('#color_container label.active').attr('data-button-color');
        $scope.booking_widget_button_font_color = $('#color_container label.active').attr('data-button-font-color');
        $scope.booking_widget_header_color = $('#color_container label.active').attr('data-header-color');
        return $scope.restyle_widget();
      }), 100);
    };
    $scope.sleep_before_update = function() {
      return setTimeout((function() {
        return $scope.update();
      }), 100);
    };
    $scope.rebuild_widget = function(code) {
      $('#WidgetPreview').empty();
      document.getElementById('WidgetPreview').innerHTML = code;
      document.dispatchEvent(new Event('rebuild'));
      return true;
    };
    $scope.change_width_slider = function() {
      $scope.booking_widget_width = parseInt($scope.booking_widget_width_tmp);
      return $scope.restyle_widget();
    };
    $scope.restyle_widget = function() {
      var height, iframe_width, mtbutton, width;
      if ($scope.booking_widget_width_responsive) {
        vertical_elem.css({
          'display': "none"
        });
      } else {
        vertical_elem.css({
          'display': "block"
        });
      }
      if (($('#mt_bg').length)) {
        $('#mt_bg')[0].style.setProperty("border-radius", $scope.booking_widget_border_radius + "px", "important");
        $('#mt_bg').css({
          "background": $scope.booking_widget_bg_color
        });
        $('#mt_calculate_cost1').css({
          "background": $scope.booking_widget_button_color,
          "color": $scope.booking_widget_button_font_color
        });
        $('#box_title').css({
          "color": $scope.booking_widget_header_color
        });
        if ($scope.booking_widget_type === 3) {
          $scope.update_width_slider(620);
        } else if ($scope.booking_widget_type === 4) {
          $scope.update_width_slider(270);
        }
        if ($scope.booking_widget_width_responsive) {
          $('#MTWidget').css({
            'width': "auto"
          });
        } else {
          $('#MTWidget').css({
            'width': $scope.booking_widget_width + "px"
          });
        }
      } else if ($scope.booking_widget_type === 1) {
        $scope.update_width_slider(800);
        height = $('#WidgetPreview>iframe').contents().find("#whitebg2").height() + 20;
        width = $scope.booking_widget_width_responsive ? "auto" : ($scope.booking_widget_width - 45) + "px";
        iframe_width = width === "auto" ? "100%" : $scope.booking_widget_width;
        $('#WidgetPreview > iframe').contents().find("#whitebg").css("width", width);
        $('#WidgetPreview > iframe').css("width", iframe_width);
        if (height > 300) {
          $('#WidgetPreview>iframe')[0].height = height;
        }
      } else {
        $scope.update_width_slider(130);
        mtbutton = $('#mtbutton');
        if ($scope.booking_widget_width_responsive) {
          mtbutton.css({
            'width': "auto"
          });
        } else {
          mtbutton.css({
            'width': $scope.booking_widget_width + "px"
          });
        }
        mtbutton.css({
          "border-radius": $scope.booking_widget_border_radius + "px",
          "background": $scope.booking_widget_button_color,
          "color": $scope.booking_widget_button_font_color
        });
      }
      return true;
    };
    $scope.update_width_slider = function(delta) {
      slider2.options.min = delta;
      $('#slider2').css({
        'padding-left': delta + "px"
      });
      if (parseInt($scope.booking_widget_width) < delta) {
        $scope.booking_widget_width = delta;
        $('#width_displayed')[0].value = delta;
      } else if (parseInt($scope.booking_widget_width) > 1000) {
        $scope.booking_widget_width = 1000;
        $('#width_displayed')[0].value = 1000;
      }
      return vertical_elem.css({
        'left': ((parseInt($scope.booking_widget_width) - delta) * 100 / (1000 - delta)) + "%"
      });
    };
    $scope.update = function() {
      $scope.send_disabled = true;
      $scope.message = 'Обновление...';
      $scope.additionally_message = '';
      $scope.status = 'info';
      return $http.put("/booking_widgets/" + $scope.booking_widget_id, {
        is_use_booking_widget: $scope.is_use_booking_widget,
        partner_enabled: $scope.partner_enabled,
        booking_widget_header_color: $scope.booking_widget_header_color,
        booking_widget_settings: $scope.booking_widget_settings,
        booking_widget_type: $scope.booking_widget_type,
        booking_widget_bg_color: $scope.booking_widget_bg_color,
        booking_widget_button_color: $scope.booking_widget_button_color,
        booking_widget_border_radius: $('#ex1').val(),
        booking_widget_width: $('#ex2').val(),
        booking_widget_width_responsive: $scope.booking_widget_width_responsive,
        booking_widget_button_font_color: $scope.booking_widget_button_font_color,
        camp_id: $('#camp_id').val()
      }).success(function(data) {
        $scope.send_disabled = false;
        $scope.status = 'success';
        $scope.message = 'Операция редактирования успешно завершена.';
        $scope.additionally_message = '';
        $scope.rebuild_widget(data.widget_code);
        $('#widget_code').val(data.widget_code);
        return $scope.restyle_widget(data);
      }).error(function(data) {
        $window.FunnySadDialog.createSadDialog('Что-то пошло не так', 'Перезагрузите страницу или попробуйте позже.');
        $scope.send_disabled = false;
        $scope.modal_status = 'danger';
        $scope.modal_message = "Произошла ошибка.";
        return $scope.modal_additionally_message = "Попробуйте еще раз или обратитесь к разработчикам.";
      });
    };
    $('#slider2').on('slideStart', function() {
      return $('#overlay').show();
    });
    $('#slider2').on('slideStop', function() {
      return $('#overlay').hide();
    });
    $('#width_displayed').change(function() {
      return $scope.restyle_widget();
    });
    slider_dragged = false;
    interval = setInterval(function() {
      if (($('#ex2Slider').length != null) && !slider_dragged) {
        clearInterval(interval);
        vertical_elem.mousedown(function() {
          slider_dragged = true;
          return $('#ws_tooltip').remove();
        });
        $('#ws_tooltip').detach().appendTo(vertical_elem);
        return $('#ws_tooltip').css({
          'display': 'block',
          'transform': 'scale(1, -1)'
        });
      }
    }, 100);
    return setInterval(function() {
      var delta;
      vertical_elem.css({
        "height": $('.jumbotron').outerHeight()
      });
      delta = 0;
      if ($scope.booking_widget_type === 1) {
        delta = 800;
      } else if ($scope.booking_widget_type === 2) {
        delta = 130;
      } else if ($scope.booking_widget_type === 3) {
        delta = 620;
      } else if ($scope.booking_widget_type === 4) {
        delta = 270;
      }
      $scope.update_width_slider(delta);
      if (!slider_dragged) {
        return $('#ws_tooltip').css({
          'top': (($('.jumbotron').outerHeight() - $('#ws_tooltip').height()) / 2) + "px"
        });
      }
    }, 330);
  });

}).call(this);
