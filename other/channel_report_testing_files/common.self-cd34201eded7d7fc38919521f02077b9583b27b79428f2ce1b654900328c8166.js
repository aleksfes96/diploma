(function() {
  jQuery(function($) {
    $.datepicker.regional["ru"] = {
      closeText: "Закрыть",
      prevText: '',
      nextText: '',
      currentText: "Сегодня",
      monthNames: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
      monthNamesShort: ["Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек"],
      dayNames: ["воскресенье", "понедельник", "вторник", "среда", "четверг", "пятница", "суббота"],
      dayNamesShort: ["вск", "пнд", "втр", "срд", "чтв", "птн", "сбт"],
      dayNamesMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
      weekHeader: "Не",
      dateFormat: "dd.mm.yy",
      firstDay: 1,
      isRTL: false,
      showMonthAfterYear: false,
      yearSuffix: ""
    };
    return $.datepicker.setDefaults($.datepicker.regional["ru"]);
  });

  window.hideScrollbar = function() {
    var newScrollbarWidth, scrollbarWidthFirst;
    scrollbarWidthFirst = $('body').outerWidth();
    $('body').css({
      overflow: 'hidden',
      boxSizing: 'border-box'
    });
    newScrollbarWidth = $('body').outerWidth() - scrollbarWidthFirst;
    return $('body').css({
      'paddingRight': newScrollbarWidth + "px",
      boxSizing: 'border-box'
    });
  };

  window.showScrollbar = function() {
    return $('body, body>header').css({
      paddingRight: '',
      overflow: '',
      boxSizing: ''
    });
  };

  $(document).ready(function() {
    var blink;
    if ($(".pay_container.green.blink")[0]) {
      blink = window.setInterval((function() {
        $(".pay_container.green.blink").toggleClass("on");
      }), 1700);
    }
    $(".pay_container.green.blink input[type=radio]").change(function() {
      clearInterval(blink);
      $(".pay_container.green.blink.on").removeClass("on");
    });
    $('.promo #count_guests').change(function() {
      return $(this).next('.select').html($(this).val());
    });
    $('.promo #count_children').change(function() {
      return $(this).next('.select').html($(this).val());
    });
    $('.showOffice:first-child .description').toggle();
    return $('.showOffice').click(function() {
      var triangle;
      triangle = $(this).find('.triangle');
      if ($(triangle).html() === "▼") {
        $(triangle).html("►");
      } else {
        $(triangle).html("▼");
      }
      return $(this).find(".description").toggle("500");
    });
  });

}).call(this);
