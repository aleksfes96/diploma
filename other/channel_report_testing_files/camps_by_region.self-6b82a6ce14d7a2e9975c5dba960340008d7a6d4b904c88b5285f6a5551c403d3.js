function set_select_by_region_id(region_id, select, data_url) {
    if (region_id == 0) {
        select.empty();
        select.append($("<option></option>")
            .attr("value", 0)
            .text('Все'));
    } else {
        $.ajax({
            url: data_url,
            type: 'POST',
            data: {
                region_id: region_id
            },
            dataType: 'json',
            success: function (data) {
                select.empty();
                select.append($("<option></option>")
                    .attr("value", 0)
                    .text('Все'));
                for (i = 0, len = data.length; i < len; i++) {
                    item = data[i];
                    select.append($("<option></option>")
                        .attr("value", item.id)
                        .text(item.name));
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                render_alert_msg('Ошибка при попытке получить данные регионов с сервера!', 'danger');
            }
        });
    }
}

$(document).ready(function() {
    $('#regions').change(function () {
        var region_id, camps_select, contractors_select;
        region_id = $('#regions option:selected').val();
        camps_select = $('#camps');
        contractors_select = $('#contractors');
        if (camps_select.length > 0)
            set_select_by_region_id(region_id, camps_select, '/regions/get_camps');
        if (contractors_select.length > 0)
            set_select_by_region_id(region_id, contractors_select, '/regions/get_contractors');
    });

    $('#contractors').change(function () {
        var contractor_id = $('#contractors option:selected').val();
        var camps_select = $('#camps');
        if (contractor_id == 0) {
            camps_select.empty();
            camps_select.append($("<option></option>")
                .attr("value", 0)
                .text('Все'));
        } else {
            $.ajax({
                url: '/contractors/get_camps',
                type: 'POST',
                data: {
                    contractor_id: contractor_id
                },
                dataType: 'json',
                success: function (data) {
                    camps_select.empty();
                    camps_select.append($("<option></option>")
                        .attr("value", 0)
                        .text('Все'));
                    for (i = 0, len = data.length; i < len; i++) {
                        item = data[i];
                        camps_select.append($("<option></option>")
                            .attr("value", item.id)
                            .text(item.name));
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    render_alert_msg('Ошибка при попытке получить данные контрагентов с сервера!');
                }
            });
        }
    })
});
