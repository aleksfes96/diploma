(function() {
  var bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  window.ImageLoader = (function() {
    function ImageLoader(options) {
      this.displayFiles = bind(this.displayFiles, this);
      this.opt = {
        fileInput: '#img_file_input',
        imgContainer: '#img_container',
        sessionHash: 'input[name=session_hash]',
        imageable_type: 'input[name=imageable_type]'
      };
      this.succes_upload_number = 0;
      this.opt = $.extend(this.opt, options);
      this.fileInput = $(this.opt.fileInput);
      this.dropBox = $(this.opt.imgContainer);
      this.fileInput.on('change', (function(_this) {
        return function(eventData) {
          _this.displayFiles(eventData.currentTarget.files);
          return $(event.currentTarget).val('');
        };
      })(this));
      this.dropBox.on('click', '.pic_delete', (function(_this) {
        return function(e) {
          var currentTarget, id, request;
          if (confirm("Вы уверены")) {
            currentTarget = $(e.currentTarget);
            id = currentTarget.data('id');
            request = $.ajax({
              url: "/pictures/" + id,
              type: "POST",
              processData: false,
              contentType: "application/json",
              dataType: "json",
              beforeSend: function(xhr) {
                return xhr.setRequestHeader("X-Http-Method-Override", "DELETE");
              }
            });
            return request.done(function(msg) {
              var dropBox;
              dropBox = _this.dropBox;
              return currentTarget.closest('.row-fluid').hide(function() {
                this.remove();
                if (dropBox.children().size() === 0) {
                  return $('#picture-title').hide();
                }
              });
            });
          }
        };
      })(this));
    }

    ImageLoader.prototype.displayFiles = function(files) {
      var imageType;
      imageType = /image.*/;
      this.files_count = files.length;
      $('#send_form').attr('disabled', 'disabled');
      $('#img_file_input').attr('disabled', 'disabled');
      return $.each(files, (function(_this) {
        return function(i, file) {
          var reader;
          if (!file.type.match(imageType)) {
            return true;
          }
          reader = new FileReader();
          reader.onload = function(e) {
            var formData;
            formData = new FormData();
            formData.append('picture[pic]', file);
            formData.append('picture[session_hash]', $(_this.opt.sessionHash).val());
            formData.append('picture[imageable_type]', $(_this.opt.imageable_type).val());
            return _this.sendImage(formData);
          };
          return reader.readAsArrayBuffer(file);
        };
      })(this));
    };

    ImageLoader.prototype.sendImage = function(formData) {
      var ajax;
      ajax = $.ajax({
        type: 'POST',
        url: '/pictures',
        data: formData,
        cache: false,
        contentType: false,
        processData: false
      });
      return ajax.success((function(_this) {
        return function(data) {
          _this.dropBox.append(data);
          _this.succes_upload_number++;
          if (_this.succes_upload_number === _this.files_count) {
            $('#send_form').removeAttr('disabled');
            $('#img_file_input').removeAttr('disabled');
            _this.succes_upload_number = 0;
            if (_this.dropBox.children().size() > 0) {
              return $('#picture-title').show();
            }
          }
        };
      })(this));
    };

    return ImageLoader;

  })();

}).call(this);
