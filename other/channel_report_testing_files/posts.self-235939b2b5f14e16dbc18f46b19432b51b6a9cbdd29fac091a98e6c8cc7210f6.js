(function() {
  $(document).ready(function() {
    new window.ImageLoader({
      fileInput: '#img_file_input_post',
      imgContainer: '#img_container_post',
      sessionHash: 'input[name=session_hash_post]',
      imageable_type: 'input[name=imageable_type_post]'
    });
    return $("#img_container_post").on('click', "input[id^='url']", function(e) {
      this.select();
      return false;
    });
  });

}).call(this);
