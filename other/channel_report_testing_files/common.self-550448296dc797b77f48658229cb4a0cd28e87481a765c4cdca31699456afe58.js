strToUrl = function(w) {
    var tr='a b v g d e j z i y k l m n o p r s t u f h c ch sh sh ~ y ~ e yu ya ~ e'.split(' ');
    var ww=''; w=w.toLowerCase();
    for(i=0; i<w.length; ++i) {
        cc=w.charCodeAt(i); ch=(cc>=1072?tr[cc-1072]:w[i]);
        ww+=ch;
    }
    return(ww.replace(/~/g,'').replace(/[^a-zA-Z0-9\-]/g,'-').replace(/[-]{2,}/gim, '-').replace( /^\-+/g, '').replace( /\-+$/g, ''));
}

linkUrlField = function(fromInput, urlInput){
    $(fromInput).change(function() {
        $(urlInput).val(strToUrl(fromInput.val())).keyup();
    });
}


$(document).ready(function(){
    $('.icon-question-sign').tooltip({placement: 'bottom', animation: false});
});
