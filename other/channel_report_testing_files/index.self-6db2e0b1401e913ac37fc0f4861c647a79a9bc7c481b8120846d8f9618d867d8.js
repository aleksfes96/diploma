(function() {
  angular.module('extranet', ['extranet.controllers', 'ng-rails-csrf', 'ui.bootstrap', 'ui.bootstrap.buttons']);

}).call(this);
