(function() {
  $(document).ready(function() {
    var additional_city_div, conf_map, conf_marker, defaultLatLng, form_params, initConfirmationMap, moveMarker, redraw_additional_cities, ref, strToUrl, toggle_second_step, validate, validateCity, validateEmail, validateLocation, validateName, validatePerson, validatePhone, validateRegion, validateType, validation;
    $("input#phone").mask("+7? (999) 999-99-99");
    $("input#phone").change();
    defaultLatLng = {
      lat: 55.755826,
      lng: 37.6173
    };
    form_params = function() {
      var params;
      return params = {
        name: $('#name').val(),
        camp_type_id: $('#type_id').val(),
        site: $('#site').val(),
        region_id: $('#region option:selected').val(),
        city_id: $('#city').val() != null ? $('#city').val() : '',
        camp_id: $('#camp_id').val(),
        lat: $('#lat').val() === defaultLatLng.lat ? null : $('#lat').val(),
        lng: $('#lng').val() === defaultLatLng.lng ? null : $('#lng').val(),
        phone: $('#phone').val(),
        email: $('#email').val(),
        person: $('#person').val(),
        url: strToUrl($('#name').val()),
        special_request_attributes: {
          message: $('#notes').val()
        },
        responsible_id: $('#responsible_id option:selected').val(),
        additional_cities: window.additional_cities
      };
    };
    initConfirmationMap = function() {
      var conf_map, conf_marker;
      conf_map = new google.maps.Map(document.getElementById('map_step_2'), {
        zoom: 4,
        center: defaultLatLng,
        gestureHandling: 'greedy',
        draggable: false,
        scrollwheel: false,
        navigationControl: false,
        scaleControl: false,
        disableDefaultUI: true
      });
      conf_marker = new google.maps.Marker({
        position: defaultLatLng,
        map: conf_map
      });
      return [conf_map, conf_marker];
    };
    moveMarker = function(lat, lng) {
      marker.setPosition(new google.maps.LatLng(lat, lng));
      return map.setCenter(new google.maps.LatLng(lat, lng));
    };
    strToUrl = function(w) {
      var cc, ch, i, tr, ww;
      tr = 'a b v g d e j z i y k l m n o p r s t u f h c ch sh sh ~ y ~ e yu ya ~ e'.split(' ');
      ww = '';
      w = w.toLowerCase();
      i = 0;
      while (i < w.length) {
        cc = w.charCodeAt(i);
        ch = cc >= 1072 ? tr[cc - 1072] : w[i];
        ww += ch;
        ++i;
      }
      return ww.replace(/~/g, '').replace(/[^a-zA-Z0-9\-]/g, '-').replace(/[-]{2,}/gim, '-').replace(/^\-+/g, '').replace(/\-+$/g, '');
    };
    validatePhone = function() {
      var objphone;
      objphone = $('input#phone');
      if ($(objphone).val().trim() === '' || !objphone.val().match(/^\+7\s\([0-9]{3}\)\s[0-9]{3}-[0-9]{2}-[0-9]{2}$/)) {
        $(objphone).addClass('input-error');
        return false;
      } else {
        $(objphone).removeClass('input-error');
        return true;
      }
    };
    validate = function(input_id, error_message) {
      var obj;
      obj = $("input#" + input_id);
      if (typeof obj.val() === 'string' && $(obj).val().trim() === '') {
        $(obj).addClass('input-error');
        $(obj).val('');
        $(obj).attr('placeholder', error_message);
        return false;
      } else {
        $(obj).removeClass('input-error');
        $(obj).attr('placeholder', null);
        return true;
      }
    };
    validateEmail = function() {
      var expr, obj;
      obj = $('input#email');
      expr = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if ($(obj).val().trim() === '' || !expr.test($(obj).val().trim())) {
        $(obj).addClass('input-error');
        $(obj).attr('placeholder', 'Укажите почту, пожалуйста');
        return false;
      } else {
        $(obj).removeClass('input-error');
        $(obj).attr('placeholder', null);
        return true;
      }
    };
    validatePerson = function() {
      return validate('person', 'Пожалуйста укажите ваше имя');
    };
    validateName = function() {
      return validate('name', 'Пожалуйста укажите название');
    };
    validateType = function() {
      return validate('type', 'Пожалуйста укажите тип объекта');
    };
    validateLocation = function() {
      var el, el1, el2;
      el = $('#open_map');
      el1 = $('#lat');
      el2 = $('#lng');
      if (el1.val().trim() === '' || el2.val().trim() === '') {
        el.addClass('input-error');
        return false;
      } else {
        el.removeClass('input-error');
        return true;
      }
    };
    validateRegion = function() {
      var el;
      el = $('select#region');
      if (el.val().trim() === '') {
        el.addClass('input-error');
        return false;
      } else {
        el.removeClass('input-error');
        return true;
      }
    };
    validateCity = function() {
      var el;
      el = $('select#city');
      if (el.val().trim() === '') {
        el.addClass('input-error');
        return false;
      } else {
        el.removeClass('input-error');
        return true;
      }
    };
    validation = function() {
      var errors;
      errors = 0;
      if (!validatePerson()) {
        errors++;
      }
      if (!validateName()) {
        errors++;
      }
      if (!validateType()) {
        errors++;
      }
      if (!validateEmail()) {
        errors++;
      }
      if (!validatePhone()) {
        errors++;
      }
      if (!validateRegion()) {
        errors++;
      }
      return errors === 0;
    };
    toggle_second_step = function() {
      var latLng;
      window.scrollTo(0, 0);
      $('#article').toggle();
      $('.inner>.form-wrapper').toggle();
      $('#step_2').toggle();
      $('h1').toggle();
      latLng = new google.maps.LatLng(marker.getPosition().lat(), marker.getPosition().lng());
      google.maps.event.trigger(conf_map, 'resize');
      conf_marker.setPosition(latLng);
      conf_map.setCenter(latLng);
      return conf_map.setZoom(11);
    };
    $('#advance').click(function() {
      var params;
      params = form_params();
      if (validation()) {
        toggle_second_step();
        $('#s2_type').text($("#type_id option:selected").text());
        $('#s2_name').text(params.name);
        $('#s2_region_name').text($('#region option:selected').text());
        $('#s2_city_name').text($('#city').val() != null ? $('#city option:selected').text() : '');
        $('#s2_person').text(params.person);
        $('#s2_email').text(params.email);
        $('#s2_phone').text(params.phone);
        $('#s2_site').text(params.site);
        $('#s2_notes').text(params.special_request_attributes.message);
        if (params.lat !== null && params.lng !== null && params.lat !== "" && params.lng !== "" && parseFloat(params.lat) !== defaultLatLng.lat && parseFloat(params.lng) !== defaultLatLng.lng) {
          $('#s2_lng,#s2_lat').parent().show();
          $('#map_step_2').show();
          google.maps.event.trigger(map, 'resize');
          $('#s2_lng').text(Number.parseFloat(params.lng).toFixed(4));
          $('#s2_lat').text(Number.parseFloat(params.lat).toFixed(4));
        } else {
          $('#s2_lng,#s2_lat').parent().hide();
          $('#map_step_2').hide();
        }
        if (params.site.length < 1) {
          $('#s2_site').parent().css('display', 'none');
        }
        if (params.special_request_attributes.message.length < 1) {
          return $('#s2_notes').parent().css('display', 'none');
        }
      }
    });
    $('#retreat').click(function() {
      return toggle_second_step();
    });
    window.additional_cities = [];
    additional_city_div = '<div class="form-row"> <div class="form-item width-one-quarter"></div> <div class="form-item width-one-quarter"> <label for="nearby_cities_ids"></label> <select name="nearby_cities[]" onchange="additional_city_changed(this)"></select> </div> <div style="vertical-align:middle; height:20px;" class="form-item width-one-quarter"> <span style="font-size:16pt; color:#888; cursor: pointer;" onclick="remove_additional_city(this)">x</span> </div> </div>';
    window.remove_additional_city = function(dom) {
      var $additional_cities_blocks, idx;
      idx = $(dom).closest('#additional_cities').find('div.form-row').index($(dom).closest('div.form-row'));
      window.additional_cities.splice(idx, 1);
      $additional_cities_blocks = $('div#additional_cities');
      $($additional_cities_blocks[0]).find('div.form-row').last().remove();
      $($additional_cities_blocks[1]).find('div.form-row').last().remove();
      return redraw_additional_cities();
    };
    $('input#add_nearby_city').click(function() {
      if (parseInt($('#region').val()) > 0) {
        additional_cities.push(0);
        return redraw_additional_cities();
      } else {
        return validateRegion();
      }
    });
    window.additional_city_changed = function(dom) {
      var $select, idx;
      $select = $(dom);
      idx = $select.closest('#additional_cities').find('select').index($select);
      window.additional_cities[idx] = parseInt($select.val());
      return redraw_additional_cities();
    };
    $('select#region').change(function() {
      window.additional_cities = [];
      return redraw_additional_cities();
    });
    redraw_additional_cities = function() {
      var $additional_cities_blocks;
      $additional_cities_blocks = $('div#additional_cities');
      if (window.additional_cities.length === 0) {
        $additional_cities_blocks.empty();
      }
      if (window.additional_cities.length > $($additional_cities_blocks[0]).find('select').length) {
        $additional_cities_blocks.append(additional_city_div);
        $($additional_cities_blocks[0]).find('select').last().html($('#city').html());
        $($additional_cities_blocks[1]).find('select').last().html($('#city').html());
      }
      return $additional_cities_blocks.each(function(block_idx, block) {
        return $(block).find('select').each(function(select_idx, select) {
          $(select).find('option[selected]').removeAttr('selected');
          return $(select).val(window.additional_cities[select_idx]);
        });
      });
    };
    redraw_additional_cities();
    $('#submit_request').click(function(e) {
      var book_new_camp;
      book_new_camp = form_params();
      e.preventDefault();
      if (validation()) {
        $(this).attr("disabled", true);
        $.ajax({
          url: '/book_new_camps',
          type: 'POST',
          data: {
            book_new_camp: book_new_camp
          },
          complete: function() {
            return $('.submit_request').attr("disabled", false);
          },
          success: function(data, textStatus, jqXHR) {
            toggle_second_step();
            $('.inner>article').html(data);
            $('.inner>h1').css('display', 'none');
            $('.inner>.form-wrapper').css('display', 'none');
            $('#shame').css('display', 'none');
            return $('#article').css({
              'box-shadow': 'none',
              'height': '416px',
              'padding': '0 20px'
            });
          },
          error: function(jqXHR, textStatus, errorThrown) {
            return FunnySadDialog.createSadDialog('Что-то пошло не так', 'Ошибка отправки заявки!\nПопробуйте отправить заявку позднее.');
          }
        });
      }
      return false;
    });
    if (document.getElementById('map_step_2')) {
      ref = initConfirmationMap(), conf_map = ref[0], conf_marker = ref[1];
    }
    $('.ui-dialog-buttonset').children().addClass('button');
    $('.ui-dialog-buttonset').css('text-align', 'center');
    $('.close_dialog').on('click', function(e) {
      return $(this).parent().dialog('close');
    });
    $('input#phone').on('blur change', function() {
      $("input#phone").val($(this).val());
      return validatePhone();
    });
    $('input#email').on('blur change keyup', function() {
      $('input#email').val($(this).val());
      return validateEmail();
    });
    $('select#region').on('blur select change keyup', function() {
      $('select#region').val($(this).val());
      $('select#city').prop('disabled', false);
      $('input#open_map').prop('disabled', false);
      return validateRegion();
    });
    $('select#city').on('blur select change keyup', function() {
      return $('select#city').val($(this).val());
    });
    $('input#person').on('blur select change keyup', function() {
      $('input#person').val($(this).val());
      return validatePerson();
    });
    $('input#name').on('blur select change keyup', function() {
      $('input#name').val($(this).val());
      return validateName();
    });
    $('select#type_id').on('blur select change keyup', function() {
      $('select#type_id').val($(this).val());
      return validateType();
    });
    $('input#site').on('blur keyup', function() {
      return $('input#site').val($(this).val());
    });
    $('input#time').on('blur keyup', function() {
      return $('input#time').val($(this).val());
    });
    $('textarea#notes').on('blur keyup', function() {
      return $('textarea#notes').val($(this).val());
    });
    $('select#responsible_id').on('blur select change keyup', function() {
      $('select#responsible_id').val($(this).val());
      return validateRegion();
    });
    return $('input#open_map').on('click', function() {
      $('#map-dialog').dialog('open');
      $('body').css('overflow', 'hidden');
      google.maps.event.trigger(map, 'resize');
      map.setCenter(new google.maps.LatLng(marker.getPosition().lat(), marker.getPosition().lng()));
      return map.setZoom(11);
    });
  });

  this.ShowCitiesByRegion = (function() {
    function ShowCitiesByRegion(region_html_id, city_html_id) {
      this.region = $('select' + region_html_id);
      this.city = $('select' + city_html_id);
      this.region.change((function(_this) {
        return function() {
          return _this.get_cities();
        };
      })(this));
    }

    ShowCitiesByRegion.prototype.insert_cities = function(data) {
      var item, j, len, option, results;
      this.city.empty();
      this.city.append(new Option('', ''));
      results = [];
      for (j = 0, len = data.length; j < len; j++) {
        item = data[j];
        option = new Option(item.name, item.id);
        $(option).attr({
          lat: item.latitude,
          lng: item.longitude
        });
        results.push(this.city.append($(option)));
      }
      return results;
    };

    ShowCitiesByRegion.prototype.get_cities = function() {
      var request, value;
      value = this.region.val();
      if (value === '') {
        this.insert_cities([]);
        return;
      }
      return request = $.ajax({
        url: '/regions/get_cities',
        type: 'POST',
        data: {
          region_id: value
        },
        success: (function(_this) {
          return function(data, textStatus, jqXHR) {
            return _this.insert_cities(data);
          };
        })(this),
        error: function(jqXHR, textStatus, errorThrown) {
          return render_alert_msg('При загрузке списка городов с сервера произошла ошибка', 'danger');
        },
        dataType: 'json'
      });
    };

    return ShowCitiesByRegion;

  })();

  $(document).ready(function() {
    var cities;
    return cities = new ShowCitiesByRegion('#region', '#city');
  });

}).call(this);
